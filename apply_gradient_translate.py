#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2023] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# Apply Gradient Translate - a couple of tricks to apply translates to gradients
# An Inkscape 1.2.1+ extension
##############################################################################

import inkex

def linear_gradient_apply_translate(self, gradient):

    # We have x1, y1, x2, y2 ( Gradient Line )
    # We can only apply translate to this
    # It's not possible to convert other transforms

    if gradient.get('gradientTransform'):
        # Check gradient is only translate
        if gradient.gradientTransform.is_translate():
            x1 = gradient.get('x1')
            y1 = gradient.get('y1')
            x2 = gradient.get('x2')
            y2 = gradient.get('y2')

            x1y1_transformed = gradient.gradientTransform.apply_to_point([x1, y1])
            x2y2_transformed = gradient.gradientTransform.apply_to_point([x2, y2])

            gradient.set('x1', x1y1_transformed[0])
            gradient.set('y1', x1y1_transformed[1])
            gradient.set('x2', x2y2_transformed[0])
            gradient.set('y2', x2y2_transformed[1])

            gradient.set('gradientTransform', None)
        else:
            inkex.errormsg(f'{gradient.get_id()} contains non translate transforms')

def radial_gradient_apply_translate(self, gradient):

    # We have cx, cy ( centre of gradient )
    # and fx, fy ( focal point of gradient )
    # We can only convert translate to cy, cy, fx, fy
    # It's not possible to convert other transforms

    if gradient.get('gradientTransform'):
        # Check gradient is only translate
        if gradient.gradientTransform.is_translate():
            cx = gradient.get('cx')
            cy = gradient.get('cy')

            fx = gradient.get('fx')
            fy = gradient.get('fy')

            cxcy_transformed = gradient.gradientTransform.apply_to_point([cx, cy])

            if fx and fy:
                fxfy_transformed = gradient.gradientTransform.apply_to_point([fx, fy])
                gradient.set('fx', fxfy_transformed[0])
                gradient.set('fy', fxfy_transformed[1])

            gradient.set('cx', cxcy_transformed[0])
            gradient.set('cy', cxcy_transformed[1])

            gradient.set('gradientTransform', None)

        else:
            inkex.errormsg(f'{gradient.get_id()} contains non translate transforms')

class ApplyGradientTranslate(inkex.EffectExtension):

    def add_arguments(self, pars):
        pass
    
    def effect(self):

        linear_gradients = self.svg.defs.xpath('svg:linearGradient')

        for linear_gradient in linear_gradients:
            linear_gradient_apply_translate(self, linear_gradient)

        radial_gradients = self.svg.defs.xpath('svg:radialGradient')

        for radial_gradient in radial_gradients:
            radial_gradient_apply_translate(self, radial_gradient)

if __name__ == '__main__':
    ApplyGradientTranslate().run()

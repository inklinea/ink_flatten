import inkex

from random import shuffle
from random import random

# Function to list object attributes
####################################

def get_attributes(self):

    attribute_string = ''
    for att in dir(self):
        try:
            attribute = (att, getattr(self, att))
            attribute_string = attribute_string + str(attribute) + '\n'
        except:
            None
            
    return attribute_string

def add_random_transform(self, element):

    # transform_types_list = [f'translate({20*random()},{20*random()})', f'rotate({360*random()})', f'scale({random()})', f'skewX({180*random()})', f'skewY({180*random()})']
    

    #transform_types_list = [f'translate({20*random()},{20*random()})', f'rotate({360*random()})', f'scale({random()})']
    
    transform_types_list = [f'translate({20*random()},{20*random()})', f'rotate({360*random()})']

    shuffle(transform_types_list)

    # inkex.errormsg(transform_types_list)

    transform_string = ''.join(transform_types_list)

    # inkex.errormsg(transform_string)

    element.set('transform', transform_string)



class AddRandomTransforms(inkex.EffectExtension):

    def add_arguments(self, pars):
        pass
    
    def effect(self):

        selection_list = self.svg.selected
        if len(selection_list) < 1:
            return

        for item in selection_list:
            add_random_transform(self, item)
        
if __name__ == '__main__':
    AddRandomTransforms().run()

#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2022] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# Ink Dbus- a dbus based inkscape extension
# An Inkscape 1.2.1+ extension
##############################################################################

from time import sleep

import gi

gi.require_version("Gio", "2.0")
from gi.repository import Gio, GLib

import warnings
warnings.filterwarnings("ignore")

class InkDbus:

    def ink_dbus_action(self, path, action, param, param_type, id_list):
        """

        :param path: the dbus path to use can be application/document/window
        :param action: the action to activate
        :param param: parameters string to pass to the action
        :param param_type: the data type of the parameter, ( string, double etc )
        :param id_list: a list of ids which can passes to another function if needed
        """
        if path == 'document':
            action_path = InkDbus.documentGroup
        elif path == 'application':
            action_path = InkDbus.applicationGroup
        elif path == 'window':
            action_path = InkDbus.windowGroup

        param = param.strip()

        if str(param) != None and param.strip().lower() != 'none' and param.strip() != '':

            # Some parameters such as export-dpi require a different type to string

            # inkex.errormsg(param_type.strip())

            if param_type.strip() == 'i':
                param_string = GLib.Variant.new_int32(int(param))
            elif param_type.strip() == 'd':
                param_string = GLib.Variant.new_double(float(param))
            elif param_type.strip() == 'b':
                if param.lower() == 'false':
                    # inkex.errormsg(param)
                    param_string = GLib.Variant.new_boolean(False)
                    # inkex.errormsg(f'param_string (false): {param_string}')
                else:
                    param_string = GLib.Variant.new_boolean('True')
                    # inkex.errormsg(f'param_string (true): {param_string}')
            else:
                param_string = GLib.Variant.new_string(param)

            action_path.activate_action(action, param_string)



        else:
            if param_type.strip() == 's':
                action_path.activate_action(action, GLib.Variant.new_string(''))
            else:
                action_path.activate_action(action, None)
                # action_path.activate_action(action, GLib.Variant.new_string(''))


## The code in start_bus is taken from the Inkscape wiki page on dbus
## https://wiki.inkscape.org/wiki/DBus

    def start_bus(self):

        try:
            bus = Gio.bus_get_sync(Gio.BusType.SESSION, None)

        except BaseException:
            # print("No DBus bus")
            exit()

        # print ("Got DBus bus")

        proxy = Gio.DBusProxy.new_sync(bus, Gio.DBusProxyFlags.NONE, None,
                                       'org.freedesktop.DBus',
                                       '/org/freedesktop/DBus',
                                       'org.freedesktop.DBus', None)

        names_list = proxy.call_sync('ListNames', None, Gio.DBusCallFlags.NO_AUTO_START, 500, None)

        # names_list is a GVariant, must unpack

        names = names_list.unpack()[0]

        # Look for Inkscape; names is a tuple.

        for name in names:
            if ('org.inkscape.Inkscape' in name):
                # print ("Found: " + name)

                break

        name = 'org.inkscape.Inkscape'

        # print ("Name: " + name)

        appGroupName = "/org/inkscape/Inkscape"
        winGroupName = appGroupName + "/window/1"
        docGroupName = appGroupName + "/document/1"

        InkDbus.applicationGroup = Gio.DBusActionGroup.get(bus, name, appGroupName)
        InkDbus.windowGroup = Gio.DBusActionGroup.get(bus, name, winGroupName)
        InkDbus.documentGroup = Gio.DBusActionGroup.get(bus, name, docGroupName)

## Start the bus

InkDbus.start_bus(None)

# Give the bus 0.5 seconds to start.

sleep(0.5)

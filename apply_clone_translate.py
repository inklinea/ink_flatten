#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2023] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# Apply Gradient Translate - a couple of tricks to apply translates to clones
# An Inkscape 1.2.1+ extension
##############################################################################

import inkex

def apply_clone_translate(self, clone):

    if clone.get('transform'):
        # Check transform is only translate
        if clone.transform.is_translate():

            clone_x = float(clone.get('x'))
            clone_y = float(clone.get('y'))

            clone_parent_id = clone.get('xlink:href').split('#')[1]

            clone_parent = self.svg.getElementById(clone_parent_id)

            clone_parent_bbox = clone_parent.bounding_box()

            x1y1_transformed = clone.transform.apply_to_point([clone_parent_bbox.left, clone_parent_bbox.top])

            clone_new_x = x1y1_transformed[0] - clone_parent_bbox.left + clone_x
            clone_new_y = x1y1_transformed[1] - clone_parent_bbox.top + clone_y

            clone.set('x', clone_new_x)
            clone.set('y', clone_new_y)

            clone.set('transform', None)
        else:
            inkex.errormsg(f'{clone.get_id()} contains non translate transforms')

def apply_backlinks_translate(self, element):

    backlinks = element.backlinks()
    for backlink in backlinks:
        if backlink.TAG == 'use':
            apply_clone_translate(self, backlink)
        # if the element is not a clone, apply all tranforms
        # else:

class ApplyCloneTranslate(inkex.EffectExtension):

    def add_arguments(self, pars):
        pass
    
    def effect(self):
        pass

        selection_list = self.svg.selected
        if len(selection_list) < 1:
            inkex.errormsg('Please select at least one object')
            return

        for selection in selection_list:
            apply_backlinks_translate(self, selection)
        
if __name__ == '__main__':
    ApplyCloneTranslate().run()

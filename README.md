# Ink Flatten

An Inkscape 1.2.1+ Extension

Appears under Extensions>Ink Flatten

Just some dbus tricks to flatten paths. 

Filters are not accounted for.

Also to convert gradient and clone translates into x, y coordinates on the clone rather than a translate.

If the clone / gradient has other transform components, it will not be processed.

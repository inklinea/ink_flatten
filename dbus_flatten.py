#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2023] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# dbus Flatten - a couple of tricks using dbus actions to flatten transforms
# An Inkscape 1.2.1+ extension
##############################################################################

import inkex

from ink_dbus import *

import sys

import warnings
warnings.filterwarnings("ignore")

# Functions to silence stderr and stdout
# Close output pipeline ( See notes at top of script )
# If they are not silenced, any messages prevent the selection passback
def set_stdout(self, state):
    import sys, os
    if state == 'off':
        sys.stdout = open(os.devnull, 'w')
    else:
        sys.stdout.close()
        sys.stdout = sys.__stdout__


def set_stderr(self, state):
    import sys, os
    if state == 'off':
        sys.stderr = open(os.devnull, 'w')
    else:
        sys.stderr.close()
        sys.stderr = sys.__stderr__

set_stdout(None, 'off')
set_stderr(None, 'off')

# Let's make a function to decend into groups
def apply_group_transforms(self, group):

    # We only want the 1st level of children
    direct_children = group.getchildren()

    if group.get('transform'):
        group_transform = group.transform

        for child in direct_children:
            # Apply the parent group transform to the child
            child.transform = group_transform @ child.transform
            child_transform_string = child.get('transform')
            InkDbus.ink_dbus_action(self, 'application', 'select-clear', '', '', None)
            InkDbus.ink_dbus_action(self, 'application', 'select-by-id', child.get_id(), 's', None)
            InkDbus.ink_dbus_action(self, 'application', 'object-set-attribute', f'transform,{child_transform_string}', 's', None)

        # Now remove the transform from the parent group
        group.set('transform', None)
        InkDbus.ink_dbus_action(self, 'application', 'select-clear', '', '', None)
        InkDbus.ink_dbus_action(self, 'application', 'select-by-id', group.get_id(), 's', None)
        InkDbus.ink_dbus_action(self, 'application', 'transform-remove', '','', None)


def flatten_path(self, element):

    if element.style.get('filter'):
        element_filter = element.style.get('filter')
    else:
        element_filter = None

    InkDbus.ink_dbus_action(self, 'application', 'select-clear', '', '', None)
    InkDbus.ink_dbus_action(self, 'application', 'select-by-id', element.get_id(), 's', None)

    InkDbus.ink_dbus_action(self, 'application', 'edit-remove-filter', '','', None)

    InkDbus.ink_dbus_action(self, 'application', 'object-to-path', '', '', None)
    InkDbus.ink_dbus_action(self, 'application', 'object-flip-horizontal', '', '', None)
    InkDbus.ink_dbus_action(self, 'application', 'object-flip-horizontal', '', '', None)

    if element_filter:
        InkDbus.ink_dbus_action(self, 'application', 'object-set-property', f'filter,{element_filter}', 's', None)



def flatten_text(self, text_element):
    InkDbus.ink_dbus_action(self, 'application', 'select-clear', '', '', None)
    InkDbus.ink_dbus_action(self, 'application', 'select-by-id', text_element.get_id(), 's', None)
    InkDbus.ink_dbus_action(self, 'application', 'object-flip-horizontal', '', '', None)
    InkDbus.ink_dbus_action(self, 'application', 'object-flip-horizontal', '', '', None)

def flatten_element_tree(self, root):

    for element in root.iter():

        if element.TAG != 'g':
            if element.get('transform'):
                if element.TAG != 'text':
                    inkex.errormsg('flattening path')
                    flatten_path(self, element)
                else:
                    flatten_text(self, element)

def get_clone_parent(self, clone):
    pass

def apply_clone_transform(self, clone):

    if clone.get('transform'):
        x1 = clone.get('x1')
        y1 = clone.get('y1')
        x2 = clone.get('x2')
        y2 = clone.get('y2')

        x1y1_transformed = clone.cloneTransform.apply_to_point([x1, y1])
        x2y2_transformed = clone.cloneTransform.apply_to_point([x2, y2])

        clone.set('x1', x1y1_transformed[0])
        clone.set('y1', x1y1_transformed[1])
        clone.set('x2', x2y2_transformed[0])
        clone.set('y2', x2y2_transformed[1])

        clone.set('cloneTransform', None)

class DbusFlatten(inkex.EffectExtension):

    def add_arguments(self, pars):

        pars.add_argument("--flatten_source_radio", type=str, dest="flatten_source_radio", default='selected')
    
    def effect(self):

        if self.options.flatten_source_radio == 'selected':
            selection_list = self.svg.selected
            if len(selection_list) < 1:
                sys.exit()
        else:
            selection_list = self.svg

        # First transfer all parent group transformations
        # onto direct children, then delete group transform

        for selected in selection_list:
            for element in selected.iter():
                if element.TAG == 'g':
                    apply_group_transforms(self, element)

        # Then force Inkscape to flatten each element
        for selected in selection_list:
            flatten_element_tree(self, selected)

        # Now exit the extension *without* returning the svg
        # from the extension system
        sys.exit()

if __name__ == '__main__':
    DbusFlatten().run()
